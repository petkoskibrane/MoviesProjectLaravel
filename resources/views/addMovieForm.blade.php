<form action="{{route('addMovie')}}" method="post">
    {{csrf_field()}}
    <label>Title</label>
    <input type="text" name="title">
    <br>
    <label>Year</label>
    <input type="text" name="year">
    <br>
    <label>Director</label>
    <input type="text" name="director">
    <br>
    <label>Actors (add more actors separated with comma)</label>
    <input type="text" name="actors">
    <br>
    <input type="submit">

</form>