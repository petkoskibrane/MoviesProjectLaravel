<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomStuff\Movie;
use App\CustomStuff\MovieManager;
class MovieController extends Controller
{
    public function showAddMovieForm(){

        return view('addMovieForm');
    }


    public function showMovies(){
        $moviemanager = new MovieManager();
        $moviemanager->loadMovies();
    }


    public function addNewMovie(Request $request){
//        dd($request->all());
        $moviesData = $request->all();


        $movieTitle = $moviesData['title'];
        $movieYear = $moviesData['year'];
        $movieDirector = $moviesData['director'];
        $moviesActors = $moviesData['actors'];

        $movie = new Movie($movieTitle,$movieYear,$movieDirector,$moviesActors);

        $movieManager = new MovieManager();

        $movieManager->addMovie($movie);


//        return view('welcome');




    }
}
