<?php

namespace App\CustomStuff;

class Movie
{
    private $title,$year,$director,$actors;


    public function __construct($title,$year,$director,$actors)
    {
        $this->title = $title;
        $this->year = $year;
        $this->director = $director;
        $this->actors = $actors;
    }



    public function getTitle()
    {
        return $this->title;
    }


    public function getYear()
    {
        return $this->year;
    }

    public function getDirector()
    {
        return $this->director;
    }


    public function getActors()
    {
        return $this->actors;
    }
}