<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/addMovieForm')->name('add-movie-form')->uses('MovieController@showAddMovieForm');

Route::post('/addNewMovie')->name('addMovie')->uses('MovieController@addNewMovie');

Route::get('/showMovies')->name('showMovies')->uses('MovieController@showMovies');